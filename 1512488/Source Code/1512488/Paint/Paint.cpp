// Paint.cpp : Defines the entry point for the application.
//
#pragma once
#include "stdafx.h"
#include "Paint.h"
#include "CShape.h"
using namespace std;

#define MAX_LOADSTRING 100
bool Drawing = FALSE;
HWND gPaintDraw;
POINT pHienHanh, pKT;
HPEN gPenDraw;
int isOpenFile = 0;
int Pen_size = 1;				
int iChoose = LINE;				
COLORREF iColor = RGB(0, 0, 0);	
vector<CShape*> gDang;
vector<CShape*> gMau;
HMENU hMenu;
POINT pRMouse;
RECT rect;

//HÀM
void ChangeToSpecial();

class CLine : public CShape
{
public:
	CLine()
	{}
	void Draw(HDC hdc)
	{
		gPenDraw = CreatePen(0, sSize, sColor);
		SelectObject(hdc, gPenDraw);
		MoveToEx(hdc, a.x, a.y, NULL);
		LineTo(hdc, b.x, b.y);
		DeleteObject(gPenDraw);
	}
	CShape* Create()
	{
		return new CLine;
	}
	void SetData(int ax, int ay, int bx, int by, COLORREF clr, int sz)
	{
		a.x = ax;
		a.y = ay;
		b.x = bx;
		b.y = by;
		sColor = clr;
		sSize = sz;
	}
	~CLine()
	{}
};

class CRectangle : public CShape
{
public:
	CRectangle()
	{
	}
	void Draw(HDC hdc)
	{
		gPenDraw = CreatePen(0, sSize, sColor);
		SelectObject(hdc, gPenDraw);
		Rectangle(hdc, a.x, a.y, b.x, b.y);
		DeleteObject(gPenDraw);
	}
	CShape* Create()
	{
		return new CRectangle;
	}
	void SetData(int ax, int ay, int bx, int by, COLORREF clr, int sz)
	{
		a.x = ax;
		a.y = ay;
		b.x = bx;
		b.y = by;
		sColor = clr;
		sSize = sz;
	}
	~CRectangle()
	{
	}
};
class CEllipse : public CShape
{
public:
	CEllipse()
	{}
	void Draw(HDC hdc)
	{
		gPenDraw = CreatePen(0, sSize, sColor);
		SelectObject(hdc, gPenDraw);
		Ellipse(hdc, a.x, a.y, b.x, b.y);
		DeleteObject(gPenDraw);
	}
	CShape* Create()
	{
		return new CEllipse;
	}
	void SetData(int ax, int ay, int bx, int by, COLORREF clr, int sz)
	{
		a.x = ax;
		a.y = ay;
		b.x = bx;
		b.y = by;
		sColor = clr;
		sSize = sz;
	}
	~CEllipse()
	{}
};


// Global Variables:
HINSTANCE hInst;                                // current instance
WCHAR szTitle[MAX_LOADSTRING];                  // The title bar text
WCHAR szWindowClass[MAX_LOADSTRING];            // the main window class name

// Forward declarations of functions included in this code module:
ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPWSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);

    // TODO: Place code here.

    // Initialize global strings
    LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
    LoadStringW(hInstance, IDC_PAINT, szWindowClass, MAX_LOADSTRING);
    MyRegisterClass(hInstance);

    // Perform application initialization:
    if (!InitInstance (hInstance, nCmdShow))
    {
        return FALSE;
    }

    HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_PAINT));

    MSG msg;

    // Main message loop:
    while (GetMessage(&msg, nullptr, 0, 0))
    {
        if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
    }

    return (int) msg.wParam;
}



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
    WNDCLASSEXW wcex;

    wcex.cbSize = sizeof(WNDCLASSEX);

    wcex.style          = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc    = WndProc;
    wcex.cbClsExtra     = 0;
    wcex.cbWndExtra     = 0;
    wcex.hInstance      = hInstance;
    wcex.hIcon          = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_PAINT));
    wcex.hCursor        = LoadCursor(nullptr, IDC_ARROW);
    wcex.hbrBackground  = (HBRUSH)(COLOR_WINDOW+1);
    wcex.lpszMenuName   = MAKEINTRESOURCEW(IDC_PAINT);
    wcex.lpszClassName  = szWindowClass;
    wcex.hIconSm        = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

    return RegisterClassExW(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   hInst = hInstance; // Store instance handle in our global variable

   HWND hWnd = CreateWindowW(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
      CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, nullptr, nullptr, hInstance, nullptr);

   if (!hWnd)
   {
      return FALSE;
   }

   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   return TRUE;
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND  - process the application menu
//  WM_PAINT    - Paint the main window
//  WM_DESTROY  - post a quit message and return
//
//

BOOL OnCreate(HWND hwnd, LPCREATESTRUCT lpCreateStruct);
void OnCommand(HWND hwnd, int id, HWND hwndCtl, UINT codeNotify);
void OnButtonDown(HWND hwnd, int dc, int x, int y, LPARAM lParam);
void OnButtonUp(HWND hwnd, int x, int wParam, LPARAM lParam);
void OnMouseMove(HWND hwnd, int x, int y, LPARAM lParam);
void OnPaint(HWND hwnd);
void OnDestroy(HWND hwnd);

LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{

	switch (message)
	{
		HANDLE_MSG(hWnd, WM_CREATE, OnCreate);
		HANDLE_MSG(hWnd, WM_COMMAND, OnCommand);
		HANDLE_MSG(hWnd, WM_PAINT, OnPaint);
		HANDLE_MSG(hWnd, WM_DESTROY, OnDestroy);
		HANDLE_MSG(hWnd, WM_MOUSEMOVE, OnMouseMove);
		HANDLE_MSG(hWnd, WM_LBUTTONDOWN, OnButtonDown);
		HANDLE_MSG(hWnd, WM_LBUTTONUP, OnButtonUp);
    default:
        return DefWindowProc(hWnd, message, wParam, lParam);
    }
    return 0;
}

// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
    UNREFERENCED_PARAMETER(lParam);
    switch (message)
    {
    case WM_INITDIALOG:
        return (INT_PTR)TRUE;

    case WM_COMMAND:
        if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
        {
            EndDialog(hDlg, LOWORD(wParam));
            return (INT_PTR)TRUE;
        }
        break;
    }
    return (INT_PTR)FALSE;
}

BOOL OnCreate(HWND hWnd, LPCREATESTRUCT lpCreateStruct)
{
	hMenu = GetMenu(hWnd);
	CheckMenuItem(hMenu, ID_DRAW_LINE, MF_CHECKED);

	gMau.push_back(new CLine);
	gMau.push_back(new CRectangle);
	return true;
}

void OnCommand(HWND hWnd, int id, HWND hwndCtl, UINT codeNotify)
{
	hMenu = GetMenu(hWnd);

	switch (id)
	{
	case ID_DRAW_LINE:
	{
		CheckMenuItem(hMenu, ID_DRAW_LINE, MF_CHECKED);
		CheckMenuItem(hMenu, ID_DRAW_RECTANGLE, MF_UNCHECKED);
		CheckMenuItem(hMenu, ID_DRAW_ELLIPSE, MF_UNCHECKED);
		iChoose = LINE;
		break;
	}
	case ID_DRAW_RECTANGLE:
	{
		CheckMenuItem(hMenu, ID_DRAW_LINE, MF_CHECKED);
		CheckMenuItem(hMenu, ID_DRAW_RECTANGLE, MF_UNCHECKED);
		CheckMenuItem(hMenu, ID_DRAW_ELLIPSE, MF_UNCHECKED);
		iChoose = RECTANGLE;
		break;
	}
	case ID_DRAW_ELLIPSE:
	{
		CheckMenuItem(hMenu, ID_DRAW_LINE, MF_CHECKED);
		CheckMenuItem(hMenu, ID_DRAW_RECTANGLE, MF_UNCHECKED);
		CheckMenuItem(hMenu, ID_DRAW_ELLIPSE, MF_UNCHECKED);
		iChoose = ELLIPSE;
		break;
	}
	case ID_FILE_NEW:
	{
		if (gDang.size() > 0)
		{
			int mb = MessageBox(hWnd, L"Tạo mới sẽ làm mất hình bạn đã vẽ. Continue?", L"MAKE NEW FILE", MB_OKCANCEL);
			if (mb == 2)			
				break;			
		}
		gDang.clear();
		isOpenFile = 0;
		InvalidateRect(hWnd, NULL, TRUE);
		break;
	}
	case IDM_EXIT:
		DestroyWindow(hWnd);
		break;
	}
}

void OnButtonDown(HWND hwnd, int dc, int x, int y, LPARAM lParam)
{
	if (!Drawing)
	{
		Drawing = TRUE;
		pHienHanh.x = x;
		pHienHanh.y = y;
	}
}

void OnButtonUp(HWND hWnd, int x, int y, LPARAM lParam)
{
	pKT.x = x;
	pKT.y = y;

	if (iChoose == LINE)
	{
		CLine* line = new CLine;
		line->SetData(pHienHanh.x, pHienHanh.y, pKT.x, pKT.y, iColor, Pen_size);
		gDang.push_back(line);
	}
	else if (iChoose == RECTANGLE)
	{
		if (lParam & MK_SHIFT)		
			ChangeToSpecial();
		CRectangle* rect = new CRectangle;
		rect->SetData(pHienHanh.x, pHienHanh.y, pKT.x, pKT.y, iColor, Pen_size);
		gDang.push_back(rect);
	}
	else
	{
		if (lParam & MK_SHIFT)		
			ChangeToSpecial();
		CEllipse* ellipse = new CEllipse;
		ellipse->SetData(pHienHanh.x, pHienHanh.y, pKT.x, pKT.y, iColor, Pen_size);
		gDang.push_back(ellipse);
	}

	Drawing = FALSE;
	InvalidateRect(hWnd, NULL, FALSE);
}


void OnMouseMove(HWND hWnd, int x, int y, LPARAM lParam)
{
	if (Drawing)
	{
		pKT.x = x;
		pKT.y = y;
		if ((lParam & MK_SHIFT) && iChoose != 1)	
			ChangeToSpecial();
		InvalidateRect(hWnd, NULL, FALSE);
	}
}

void OnPaint(HWND hWnd)
{
	PAINTSTRUCT ps;
	HDC hdc = BeginPaint(hWnd, &ps);
	HBRUSH hbr = GetStockBrush(NULL_BRUSH);
	HPEN hpen = CreatePen(0, Pen_size, iColor);

	GetClientRect(hWnd, &rect);
	HDC vHdc = CreateCompatibleDC(hdc);
	HBITMAP vBitmap = CreateCompatibleBitmap(hdc, rect.right, rect.bottom);
	SelectObject(vHdc, vBitmap);
	FillRect(vHdc, &rect, (HBRUSH)(COLOR_WINDOW + 1));

	SelectObject(vHdc, hbr);
	SelectObject(vHdc, hpen);

	for (int i = 0; i < gDang.size(); i++) {
		gDang[i]->Draw(vHdc);
	}

	if (Drawing) {
		if (iChoose == LINE)
		{
			MoveToEx(vHdc, pHienHanh.x, pHienHanh.y, NULL);
			LineTo(vHdc, pKT.x, pKT.y);
		}
		else if (iChoose == RECTANGLE)
		{
			Rectangle(vHdc, pHienHanh.x, pHienHanh.y, pKT.x, pKT.y);
		}
		else
		{
			Ellipse(vHdc, pHienHanh.x, pHienHanh.y, pKT.x, pKT.y);
		}
	}

	BitBlt(hdc, 0, 0, rect.right, rect.bottom, vHdc, 0, 0, SRCCOPY);
	DeleteObject(hpen);
	EndPaint(hWnd, &ps);
}

void OnDestroy(HWND hwnd)
{
	PostQuitMessage(0);
}


void ChangeToSpecial()
{
	if (abs(pHienHanh.x - pKT.x) > abs(pHienHanh.y - pKT.y))
	{
		if (pHienHanh.x > pKT.x)
			pKT.x = pHienHanh.x - abs(pHienHanh.y - pKT.y);
		else
			pKT.x = pHienHanh.x + abs(pHienHanh.y - pKT.y);
	}
	else
	{
		if (pHienHanh.y > pKT.y)
			pKT.y = pHienHanh.y - abs(pHienHanh.x - pKT.x);
		else
			pKT.y = pHienHanh.y + abs(pHienHanh.x - pKT.x);
	}
}
