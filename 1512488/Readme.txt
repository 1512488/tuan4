Họ và tên : TRẦN NHẬT TÂM
MSSV : 1512488
 
PAINT USING GDI

Những công việc đã làm được :
	*Xây dựng chương trình vẽ 5 loại hình cơ bản:

		1. Đường thẳng (line). Dùng hàm MoveToEx và LineTo.

		2. Hình chữ nhật (rectangle). Dùng hàm Rectangle. Nếu giữ phím Shift sẽ vẽ hình vuông (Square)

		3. Hình Ellipse. Dùng hàm Ellipse. Nếu giữ phím Shift sẽ vẽ hình tròn (Circle)
Link Youtube : https://youtu.be/sMIHEnA68vc
Link repo :
