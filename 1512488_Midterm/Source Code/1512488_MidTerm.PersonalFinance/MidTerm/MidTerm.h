#pragma once

#include "resource.h"
#include <vector>
#include <fstream>
#include <locale>
#include <codecvt>
#include <string>
#include <commctrl.h>
#include "Item.h"
#include <windowsx.h>

#pragma comment(linker,"\"/manifestdependency:type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")
#pragma comment(lib, "ComCtl32.lib")

#define MAX_LOADSTRING 100
#define FILE_PATH		L"chitieu.txt"

#define VK_CHAR_L		0x4C
#define VK_CHAR_N		0x4E

HINSTANCE hInst;
HWND g_hWnd;
WCHAR szTitle[MAX_LOADSTRING];
WCHAR szWindowClass[MAX_LOADSTRING];
WCHAR types[6][25] =
{
	L"Ăn uống", L"Di chuyển", L"Nhà cửa",
	L"Xe cộ", L"Nhu yếu phẩm", L"Dịch vụ",
};
COLORREF mau[7] = { RGB(231, 76, 60), RGB(52, 152, 219), RGB(39, 174, 96), RGB(142, 68, 173), RGB(211, 84, 0)
					,RGB( 44, 62, 80 ), RGB(255,255,255) };
int X_hh[7];
int Y_hh;
long long* typeMoney;
int g_ItemCount = 0;

HWND hcomboBox, hmoneyInput, hdescriptionInput, htotalMoney;
HWND hAddB;
HWND g_hList;
HWND  hperA, hperB, hperC, hperD, hperE, hperF;
HWND htinhType, hwelcome;

long long g_totalMoney = 0;
bool isAdding = false;
std::vector<CItem*> listMuc;
int SelectedIndex_hh = 0;


HWND taoDS(long lExtStyle, HWND parentWnd, long ID, HINSTANCE hParentInst, int x, int y, int nWidth, int nHeight, long lStyle);
bool themItemToDS(HWND m_hListview, int mItemCount);
void vietDSItemToFile(std::wstring path);
void loadDSItemFromFile(std::wstring path);
void loadItemToList_all(HWND m_hListview);
void static_dr(HDC hdc);
void line_dr(HDC hdc, int x1, int y1, int x2, int y2, COLORREF color);
void fillHCN(HDC hdc, int x1, int y1, int x2, int y2, COLORREF color);
void setWDText(HWND hWnd, long long value, std::wstring textBefore, std::wstring textAfter); 
int getLIndex(CItem* item); 
void choInputWD();
void camInputWindow();
int xoaTD(int x, int y); 
void ThugomRac(); 
void QtThemItem(HWND m_hDS); 

