//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by MidTerm.rc
//
#define IDC_MYICON                      2
#define IDD_MIDTERM_DIALOG              102
#define IDS_APP_TITLE                   103
#define IDD_ABOUTBOX                    103
#define IDM_ABOUT                       104
#define IDM_EXIT                        105
#define IDI_MIDTERM                     107
#define IDI_SMALL                       108
#define IDC_MIDTERM                     109
#define IDC_BUTTON_ADD                  111
#define IDI_DELETE                      114
#define IDC_COMBOBOX                    115
#define IDC_GROUPBOX                    116
#define IDC_STATIC_1                    117
#define IDR_MAINFRAME                   128
#define ID_FILE_NEW                     130
#define IDD_DELETE_DIALOG                199
#define IDL_LISTVIEW                    131
#define ID_FILE_DELETE                   132
#define IDC_STATIC                      -1

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NO_MFC                     1
#define _APS_NEXT_RESOURCE_VALUE        131
#define _APS_NEXT_COMMAND_VALUE         32773
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           110
#endif
#endif
