// MidTerm.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "MidTerm.h"

// Forward declarations of functions included in this code module:
ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK Clear(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam);


int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPWSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);

    // TODO: Place code here.

    // Initialize global strings
    LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
    LoadStringW(hInstance, IDC_MIDTERM, szWindowClass, MAX_LOADSTRING);
    MyRegisterClass(hInstance);

    // Perform application initialization:
    if (!InitInstance (hInstance, nCmdShow))
    {
        return FALSE;
    }

    HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_MIDTERM));

    MSG msg;

    // Main message loop:
    while (GetMessage(&msg, nullptr, 0, 0))
    {
        if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
    }

    return (int) msg.wParam;
}



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
    WNDCLASSEXW wcex;

    wcex.cbSize = sizeof(WNDCLASSEX);

    wcex.style          = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc    = WndProc;
    wcex.cbClsExtra     = 0;
    wcex.cbWndExtra     = 0;
    wcex.hInstance      = hInstance;
    wcex.hIcon          = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_MIDTERM));
    wcex.hCursor        = LoadCursor(nullptr, IDC_ARROW);
    wcex.hbrBackground  = (HBRUSH)(COLOR_WINDOW+1);
    wcex.lpszMenuName   = MAKEINTRESOURCEW(IDC_MIDTERM);
    wcex.lpszClassName  = szWindowClass;
    wcex.hIconSm        = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

    return RegisterClassExW(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   hInst = hInstance; // Store instance handle in our global variable

   HWND hWnd = CreateWindowEx(0, szWindowClass, L"Quản Lí Chi Tiêu",
	   WS_OVERLAPPEDWINDOW,
	   CW_USEDEFAULT, 0, 700, 550, NULL, NULL, hInstance, NULL);

   if (!hWnd)
   {
      return FALSE;
   }

   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   return TRUE;
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND  - process the application menu
//  WM_PAINT    - Paint the main window
//  WM_DESTROY  - post a quit message and return
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	int wmID, wmev;
	PAINTSTRUCT paintstruct;
	HDC hdc = NULL;

    switch (message)
    {
	case WM_CREATE:
	{
		INITCOMMONCONTROLSEX initcontrol;
		initcontrol.dwSize = sizeof(initcontrol);
		initcontrol.dwICC = ICC_WIN95_CLASSES;
		InitCommonControlsEx(&initcontrol);

		hdc = GetDC(hWnd);
		g_hWnd = hWnd;

		HFONT hFont;

		hFont = CreateFont(16, 0, 0, 0, FW_SEMIBOLD, FALSE, FALSE, FALSE, ANSI_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_SWISS, L"Segoe UI");

		hAddB = CreateWindowEx(0, L"BUTTON", L"THÊM", WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON, 530, 41, 121, 33, hWnd, (HMENU)IDC_BUTTON_ADD, hInst, NULL);
		SendMessage(hAddB, WM_SETFONT, WPARAM(hFont), TRUE);

		hFont = CreateFont(16, 0, 0, 0, FW_DONTCARE, FALSE, FALSE, FALSE, ANSI_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_SWISS, L"Segoe UI");
		hcomboBox = CreateWindowEx(0, WC_COMBOBOX, TEXT(""),
			CBS_DROPDOWNLIST | CBS_HASSTRINGS | WS_CHILD | WS_OVERLAPPED | WS_VISIBLE,
			45, 51, 140, 110, hWnd, NULL, hInst,
			NULL);
		SendMessage(hcomboBox, WM_SETFONT, WPARAM(hFont), TRUE);

		hmoneyInput = CreateWindowEx(0, L"EDIT", L"", WS_CHILD | WS_VISIBLE | WS_BORDER | ES_NUMBER, 370, 51, 140, 25, hWnd, NULL, hInst, NULL);
		SendMessage(hmoneyInput, WM_SETFONT, WPARAM(hFont), TRUE);
		SendMessage(hmoneyInput, EM_SETCUEBANNER, TRUE, (LPARAM)L"Nhập số tiền...");

		hdescriptionInput = CreateWindowEx(0, L"EDIT", L"Ăn sáng...", WS_CHILD | WS_VISIBLE | WS_BORDER | ES_MULTILINE | ES_AUTOVSCROLL, 210, 51, 140, 25, hWnd, NULL, hInst, NULL);
		SendMessage(hdescriptionInput, WM_SETFONT, WPARAM(hFont), TRUE);

		HWND hGroupboxA = CreateWindowEx(0, L"BUTTON", L"THÊM MỘT LOẠI CHI TIÊU", WS_CHILD | WS_VISIBLE | BS_GROUPBOX | WS_GROUP, 21, 21, 641, 70, hWnd, (HMENU)IDC_GROUPBOX, hInst, NULL);
		SendMessage(hGroupboxA, WM_SETFONT, WPARAM(hFont), TRUE);


		HWND hGroupboxB = CreateWindowEx(0, L"BUTTON", L"DANH SÁCH LOẠI CHI TIÊU", WS_CHILD | WS_VISIBLE | BS_GROUPBOX | WS_GROUP, 21, 100, 641, 220, hWnd, (HMENU)IDC_GROUPBOX, hInst, NULL);
		SendMessage(hGroupboxB, WM_SETFONT, WPARAM(hFont), TRUE);

		long extStyle = WS_EX_CLIENTEDGE;
		long style = LVS_REPORT | LVS_ICON | LVS_EDITLABELS | LVS_SHOWSELALWAYS;

		g_hList = taoDS(extStyle, hWnd, IDL_LISTVIEW, hInst, 41, 120, 610, 190, style);


		HWND hGroupboxC = CreateWindowEx(0, L"BUTTON", L"THỐNG KÊ CHI TIÊU", WS_CHILD | WS_VISIBLE | BS_GROUPBOX | WS_GROUP, 21, 331, 641, 141, hWnd, (HMENU)IDC_GROUPBOX, hInst, NULL);
		SendMessage(hGroupboxC, WM_SETFONT, WPARAM(hFont), TRUE);

		htotalMoney = CreateWindowEx(0, L"EDIT", L"", WS_CHILD | WS_VISIBLE | WS_BORDER | ES_READONLY, 316, 361, 121, 21, hWnd, NULL, hInst, NULL);
		SendMessage(htotalMoney, WM_SETFONT, WPARAM(hFont), TRUE);

		hFont = CreateFont(16, 0, 0, 0, FW_DONTCARE, FALSE, TRUE, FALSE, ANSI_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_SWISS, L"Segoe UI");
		HWND hTemp = CreateWindowEx(0, L"STATIC", L"Tổng tiền", WS_CHILD | WS_VISIBLE, 246, 363, 61, 16, hWnd, NULL, hInst, NULL);
		SendMessage(hTemp, WM_SETFONT, WPARAM(hFont), TRUE);

		HWND hTemp2 = CreateWindowEx(0, L"EDIT", L"BIỂU ĐỒ", WS_CHILD | WS_VISIBLE, 31, 375, 76, 21, hWnd, NULL, hInst, NULL);
		SendMessage(hTemp, WM_SETFONT, WPARAM(hFont), TRUE);


		hFont = CreateFont(14, 0, 0, 0, FW_DONTCARE, FALSE, FALSE, FALSE, ANSI_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_SWISS, L"Segoe UI");
		htinhType = CreateWindowEx(0, L"STATIC", L"", WS_CHILD | WS_VISIBLE, 411, 450, 231, 16, hWnd, (HMENU)NULL, hInst, NULL);
		SendMessage(htinhType, WM_SETFONT, WPARAM(hFont), TRUE);


		hFont = CreateFont(16, 0, 0, 0, FW_SEMIBOLD, FALSE, FALSE, FALSE, ANSI_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_SWISS, L"Segoe UI");

		hperA = CreateWindowEx(0, L"STATIC", L"10%", WS_CHILD, 21, 398, 31, 16, hWnd, (HMENU)NULL, hInst, NULL);
		SendMessage(hperA, WM_SETFONT, WPARAM(hFont), TRUE);

		hperB = CreateWindowEx(0, L"STATIC", L"10%", WS_CHILD, 21, 398, 31, 16, hWnd, (HMENU)NULL, hInst, NULL);
		SendMessage(hperB, WM_SETFONT, WPARAM(hFont), TRUE);

		hperC = CreateWindowEx(0, L"STATIC", L"10%", WS_CHILD, 21, 398, 31, 16, hWnd, (HMENU)NULL, hInst, NULL);
		SendMessage(hperC, WM_SETFONT, WPARAM(hFont), TRUE);

		hperD = CreateWindowEx(0, L"STATIC", L"10%", WS_CHILD, 21, 398, 31, 16, hWnd, (HMENU)NULL, hInst, NULL);
		SendMessage(hperD, WM_SETFONT, WPARAM(hFont), TRUE);

		hperE = CreateWindowEx(0, L"STATIC", L"10%", WS_CHILD, 21, 398, 31, 16, hWnd, (HMENU)NULL, hInst, NULL);
		SendMessage(hperE, WM_SETFONT, WPARAM(hFont), TRUE);

		hperF = CreateWindowEx(0, L"STATIC", L"10%", WS_CHILD, 21, 398, 31, 16, hWnd, (HMENU)NULL, hInst, NULL);
		SendMessage(hperF, WM_SETFONT, WPARAM(hFont), TRUE);

		hFont = CreateFont(45, 0, 0, 0, FW_SEMIBOLD, FALSE, FALSE, FALSE, ANSI_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_SWISS, L"Segoe UI");

		hwelcome = CreateWindowEx(0, L"STATIC", L"QUẢN LÍ CHI TIÊU", WS_CHILD, 201, 398, 301, 51, hWnd, (HMENU)NULL, hInst, NULL);
		SendMessage(hwelcome, WM_SETFONT, WPARAM(hFont), TRUE);

		TCHAR A[16];
		int  j = 0;

		memset(&A, 0, sizeof(A));

		for (j = 0; j < 6; j += 1)
		{
			wcscpy_s(A, sizeof(A) / sizeof(TCHAR), (TCHAR*)types[j]);
			SendMessage(hcomboBox, (UINT)CB_ADDSTRING, (WPARAM)0, (LPARAM)A);
		}

		SendMessage(hcomboBox, CB_SETCURSEL, (WPARAM)0, (LPARAM)0);

		loadDSItemFromFile(FILE_PATH);
		loadItemToList_all(g_hList);
		g_ItemCount = listMuc.size();

		camInputWindow();

		setWDText(htotalMoney, g_totalMoney, L"", L"");
	}
	break;
    case WM_COMMAND:
		wmID = LOWORD(wParam);
		wmev = HIWORD(wParam);

		if (wmev == CBN_SELCHANGE)
		{
			WCHAR buffer[25];
			int i = SendMessage(hcomboBox, CB_GETCURSEL, NULL, NULL);
			wsprintf(buffer, L"%s...", types[i]);
			SetWindowText(hdescriptionInput, buffer);
		}

		switch (wmID)
		{
		case IDC_BUTTON_ADD:
			QtThemItem(g_hList);
			break;
		case ID_FILE_NEW:
			QtThemItem(g_hList);
			break;
		case IDM_ABOUT:
			DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
			break;
		case ID_FILE_DELETE:
			DialogBox(hInst, MAKEINTRESOURCE(IDD_DELETE_DIALOG), hWnd, Clear);
			break;
		case IDM_EXIT:
			DestroyWindow(hWnd);
			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
		}
		break;
	case WM_CTLCOLORSTATIC:
	{
		wmID = LOWORD(wParam);
		HDC hdcStatic = (HDC)wParam;
		COLORREF color;

		if ((HWND)lParam == hperA)
		{
			color = RGB(231, 76, 60);
		}
		else if ((HWND)lParam == hperB)
		{
			color = RGB(52, 152, 219);
		}
		else if ((HWND)lParam == hperC)
		{
			color = RGB(39, 174, 96);
		}
		else if ((HWND)lParam == hperD)
		{
			color = RGB(142, 68, 173);
		}
		else if ((HWND)lParam == hperE)
		{
			color = RGB(211, 84, 0);
		}
		else if ((HWND)lParam == hperF)
		{
			color = RGB(44, 62, 80);
		}
		else if ((HWND)lParam == hwelcome)
		{
			SetTextColor(hdcStatic, RGB(52, 152, 219));
			return (BOOL)GetSysColorBrush(COLOR_WINDOW);
		}
		else
		{
			return (BOOL)GetSysColorBrush(COLOR_WINDOW);
		}
		SetTextColor(hdcStatic, RGB(255, 255, 255));
		SetBkColor(hdcStatic, color);

		return (BOOL)GetSysColorBrush(COLOR_WINDOW);
	}
	case WM_NOTIFY:
		switch (((NMHDR*)lParam)->code)
		{
		case NM_CLICK:
		{
			int pos = ListView_GetNextItem(g_hList, -1, LVNI_SELECTED);
			if (pos >= 0 && pos < listMuc.size())
			{
				SelectedIndex_hh = pos;
				CItem* item = listMuc[pos];
				setWDText(hmoneyInput, item->mTien, L"", L"");
				SetWindowText(hdescriptionInput, item->mMoTa.c_str());
				SendMessage(hcomboBox, CB_SETCURSEL, WPARAM(getLIndex(item)), NULL);
			}
		}
		break;

		case NM_DBLCLK:
			break;
		default:
			break;
		}
		break;
    case WM_PAINT:
		hdc = BeginPaint(hWnd, &paintstruct);
		// TODO: Add any drawing code here...
		static_dr(hdc);
		fillHCN(hdc, 401, 436, 406, 431 + (5 * 30 / 6), mau[6]);
		EndPaint(hWnd, &paintstruct);
	break;
	case WM_DESTROY:
	{
		vietDSItemToFile(FILE_PATH);
		ThugomRac();
		PostQuitMessage(0);
	}
	break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}

INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
    UNREFERENCED_PARAMETER(lParam);
    switch (message)
    {
    case WM_INITDIALOG:
        return (INT_PTR)TRUE;

    case WM_COMMAND:
        if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
        {
            EndDialog(hDlg, LOWORD(wParam));
            return (INT_PTR)TRUE;
        }
        break;
    }
    return (INT_PTR)FALSE;
}

INT_PTR CALLBACK Clear(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	HWND hParent;
	HWND hWnd;
	RECT rect;
	switch (message)
	{
	case WM_INITDIALOG:
		if ((hParent = GetParent(hDlg)) == NULL)
		{
			hParent = GetDesktopWindow();
		}

		GetWindowRect(hParent, &rect);
		SetWindowPos(hDlg, HWND_TOP, rect.left + 700 / 2 - 250, rect.top + 550 / 2 - 100,0, 0, SWP_NOSIZE);

		return (INT_PTR)TRUE;

	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK)
		{
			g_totalMoney = 0;
			g_ItemCount = 0;
			ListView_DeleteAllItems(g_hList);
			setWDText(htotalMoney, 0, L"", L"");
			listMuc.clear();
			RedrawWindow(g_hWnd, NULL, NULL, RDW_INTERNALPAINT | RDW_UPDATENOW | RDW_ERASENOW | RDW_INVALIDATE);
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		else if (LOWORD(wParam) == IDCANCEL)
		{
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		break;
	}
	return (INT_PTR)FALSE;
}

HWND taoDS(long lExtStyle, HWND parentWnd, long ID, HINSTANCE hParentInst, int x, int y, int nWidth, int nHeight, long lStyle)
{
	HWND m_hList = CreateWindowEx(lExtStyle, WC_LISTVIEW, _T("List View"),
		WS_CHILD | WS_VISIBLE | WS_VSCROLL | WS_TABSTOP | lStyle,
		x, y, nWidth, nHeight, parentWnd, (HMENU)ID, hParentInst, NULL);
	LVCOLUMN lvColumn;
	lvColumn.mask = LVCF_FMT | LVCF_TEXT | LVCF_WIDTH;
	lvColumn.fmt = LVCFMT_LEFT;
	lvColumn.cx = 180;
	lvColumn.pszText = _T("Loại chi tiêu");
	ListView_InsertColumn(m_hList, 0, &lvColumn);
	lvColumn.fmt = LVCFMT_LEFT;
	lvColumn.cx = 245;
	lvColumn.pszText = _T("Nội dung");
	ListView_InsertColumn(m_hList, 2, &lvColumn);
	lvColumn.fmt = LVCFMT_LEFT;
	lvColumn.pszText = _T("Số tiền");
	lvColumn.cx = 180;
	ListView_InsertColumn(m_hList, 1, &lvColumn);
	return m_hList;
}

bool themItemToDS(HWND m_hListview, int mItemCount)
{
	CItem* muc = new CItem();
	WCHAR* buf;
	int len = GetWindowTextLength(hdescriptionInput);
	if (len > 0)
	{
		buf = new WCHAR[len + 1];
		GetWindowText(hdescriptionInput, buf, len + 1);
		muc->mMoTa = std::wstring(buf);
	}
	else
	{
		MessageBox(g_hWnd, L"Nội dung đang trống!", L"Thông báo", MB_ICONWARNING | MB_OK);
		return false;
	}
	len = GetWindowTextLength(hmoneyInput);
	if (len > 0)
	{
		buf = new WCHAR[len + 1];
		GetWindowText(hmoneyInput, buf, len + 1);
		muc->mTien = _wtoi64(buf);
	}
	else
	{
		MessageBox(g_hWnd, L"Số tiền đang trống!", L"Thông báo", MB_ICONWARNING | MB_OK);
		return false;
	}

	buf = new WCHAR[20];
	GetWindowText(hcomboBox, buf, 20);
	wcscpy_s(muc->mLoai, buf);

	listMuc.push_back(muc);

	g_totalMoney += muc->mTien;

	LV_ITEM lv;

	lv.mask = LVIF_TEXT | LVIF_PARAM | LVIF_IMAGE;

	lv.iItem = mItemCount;
	lv.iSubItem = 0;
	lv.pszText = muc->mLoai;
	
	ListView_InsertItem(m_hListview, &lv);

	lv.mask = LVIF_TEXT;

	lv.iSubItem = 1;
	buf = new WCHAR[20];
	wsprintf(buf, L"%I64d", muc->mTien);
	lv.pszText = buf;
	ListView_SetItem(m_hListview, &lv); 

	lv.iSubItem = 2;
	lv.pszText = (WCHAR*)muc->mMoTa.c_str();
	ListView_SetItem(m_hListview, &lv);

	setWDText(htotalMoney, g_totalMoney, L"", L"");

	RedrawWindow(g_hWnd, NULL, NULL, RDW_INTERNALPAINT | RDW_UPDATENOW | RDW_ERASENOW | RDW_INVALIDATE);

	return true;
}


void vietDSItemToFile(std::wstring path)
{
	const std::locale utf8_locale = std::locale(std::locale(), new std::codecvt_utf8<wchar_t>());
	std::wofstream file(path);
	file.imbue(utf8_locale);

	file << g_totalMoney << std::endl;

	for (int i = 0; i < listMuc.size(); i++)
	{
		file << std::wstring(listMuc[i]->mLoai) << std::endl;
		file << listMuc[i]->mTien << std::endl;
		file << std::wstring(listMuc[i]->mMoTa) << std::endl;
	}
	file.close();
}

void loadDSItemFromFile(std::wstring path)
{
	const std::locale utf8_locale = std::locale(std::locale(), new std::codecvt_utf8<wchar_t>());
	std::wfstream file;
	file.imbue(utf8_locale);
	file.open(path, std::ios::in);

	std::wstring buf;
	if (file.is_open())
	{
		if (getline(file, buf))
		{
			g_totalMoney = _wtoi64(buf.c_str());
		}

		while (getline(file, buf))
		{
			CItem* item = new CItem();

			wcscpy_s(item->mLoai, buf.c_str());
			getline(file, buf);
			item->mTien = _wtoi64(buf.c_str());
			getline(file, buf);
			item->mMoTa = buf;

			listMuc.push_back(item);
		}
	}

	file.close();
}

void loadItemToList_all(HWND m_hListview)
{
	LV_ITEM lv;
	WCHAR* buffer = new WCHAR[20];

	for (int i = 0; i < listMuc.size(); i++)
	{
		lv.mask = LVIF_TEXT | LVIF_PARAM | LVIF_IMAGE;

		lv.iItem = i;
		lv.iSubItem = 0;
		lv.pszText = listMuc[i]->mLoai;
		ListView_InsertItem(m_hListview, &lv);

		lv.mask = LVIF_TEXT;

		lv.iSubItem = 1;
		buffer = new WCHAR[20];
		wsprintf(buffer, L"%I64d", listMuc[i]->mTien);
		lv.pszText = buffer;
		ListView_SetItem(m_hListview, &lv);

		lv.iSubItem = 2;
		lv.pszText = (WCHAR*)listMuc[i]->mMoTa.c_str();
		ListView_SetItem(m_hListview, &lv);
	}
}

void static_dr(HDC hdc)
{
	if (g_totalMoney <= 0)
	{
		fillHCN(hdc, 41, 396, 41 + 600,  396 + 30, mau[6]);
		ShowWindow(hperA, SW_HIDE);
		ShowWindow(hperB, SW_HIDE);
		ShowWindow(hperC, SW_HIDE);
		ShowWindow(hperD, SW_HIDE);
		ShowWindow(hperE, SW_HIDE);
		ShowWindow(hperF, SW_HIDE);
		ShowWindow(hwelcome, SW_SHOW);
		ShowWindow(htinhType, SW_HIDE);
		return;
	}

	ShowWindow(hwelcome, SW_HIDE);

	typeMoney = new long long[6];
	for (int i = 0; i < 6; i++)
	{
		typeMoney[i] = 0;
	}
	WCHAR buffer[5];
	Y_hh = 21 + 375;
	X_hh[7];
	X_hh[0] = 21 + 20;
	float percent;
	for (int i = 0; i < listMuc.size(); i++)
	{
		typeMoney[getLIndex(listMuc[i])] += listMuc[i]->mTien;
	}

	percent = (typeMoney[0] * 1.0 / g_totalMoney); 
	X_hh[1] = X_hh[0] + percent*600;
	if (percent >= 0.07)
	{
		ShowWindow(hperA, SW_SHOW);
		MoveWindow(hperA, X_hh[0] + percent*600 / 2 - 15, Y_hh + 7, 27, 15, FALSE);
		setWDText(hperA, int(percent * 100), L"", L"%");
	}
	else
	{
		ShowWindow(hperA, SW_HIDE);
	}
	percent = (typeMoney[1] * 1.0 / g_totalMoney);
	X_hh[2] = X_hh[1] + percent*600;
	if (percent >= 0.07)
	{
		MoveWindow(hperB, X_hh[1] + percent* 600/ 2 - 15, Y_hh + 7, 27, 15, FALSE);
		ShowWindow(hperB, SW_SHOW);
		setWDText(hperB, int(percent * 100), L"", L"%");
	}
	else
	{
		ShowWindow(hperB, SW_HIDE);
	}

	percent = (typeMoney[2] * 1.0 / g_totalMoney);
	X_hh[3] = X_hh[2] + percent*600;
	if (percent >= 0.07)
	{
		ShowWindow(hperC, SW_SHOW);
		MoveWindow(hperC, X_hh[2] + percent*600 / 2 - 15, Y_hh + 7, 27, 15, FALSE);
		setWDText(hperC, int(percent * 100), L"", L"%");
	}
	else
	{
		ShowWindow(hperC, SW_HIDE);
	}

	percent = (typeMoney[3] * 1.0 / g_totalMoney);
	X_hh[4] = X_hh[3] + percent*600;
	if (percent >= 0.07)
	{
		MoveWindow(hperD, X_hh[3] + percent*600 / 2 - 15, Y_hh + 7, 27, 15, FALSE);
		ShowWindow(hperD, SW_SHOW);
		setWDText(hperD, int(percent * 100), L"", L"%");
	}
	else
	{
		ShowWindow(hperD, SW_HIDE);
	}

	percent = (typeMoney[4] * 1.0 / g_totalMoney);
	X_hh[5] = X_hh[4] + percent*600;
	if (percent >= 0.07)
	{
		MoveWindow(hperE, X_hh[4] + percent*600 / 2 - 15, Y_hh + 7, 27, 15, FALSE);
		ShowWindow(hperE, SW_SHOW);
		setWDText(hperE, int(percent * 100), L"", L"%");
	}
	else
	{
		ShowWindow(hperE, SW_HIDE);
	}

	percent = (typeMoney[5] * 1.0 / g_totalMoney);
	X_hh[6] = X_hh[5] + percent*600;
	if (percent >= 0.07)
	{
		MoveWindow(hperF, X_hh[5] + percent*600 / 2 - 15, Y_hh + 7, 27, 15, FALSE);
		ShowWindow(hperF, SW_SHOW);
		setWDText(hperF, int(percent * 100), L"", L"%");
	}
	else
	{
		ShowWindow(hperF, SW_HIDE);
	}

	for (int i = 0; i < 6; i++)
	{
		fillHCN(hdc, X_hh[i], Y_hh, X_hh[i + 1], Y_hh + 30, mau[i]);
	}
}

void choInputWD()
{
	EnableWindow(hcomboBox, true);
	EnableWindow(hmoneyInput, true);
	EnableWindow(hdescriptionInput, true);
	SetWindowText(hAddB, L"LƯU");
}

void camInputWindow()
{
	EnableWindow(hcomboBox, false);
	EnableWindow(hmoneyInput, false);
	EnableWindow(hdescriptionInput, false);
	SetWindowText(hAddB, L"THÊM");
}

void line_dr(HDC hdc, int x1, int y1, int x2, int y2, COLORREF color)
{
	HPEN hpen = CreatePen(PS_SOLID, 10, color);
	SelectObject(hdc, hpen);

	MoveToEx(hdc, x1, y1, NULL);
	LineTo(hdc, x2, y2);
}

void fillHCN(HDC hdc, int a1, int b1, int a2, int b2, COLORREF color)
{
	RECT* r = new RECT;
	r->left = a1;
	r->top = b1;
	r->right = a2;
	r->bottom = b2;

	HBRUSH hbrush = CreateSolidBrush(color);

	FillRect(hdc, r, hbrush);
}

void setWDText(HWND hWnd, long long value, std::wstring textBefore, std::wstring textAfter)
{
	WCHAR buffer[255];
	if (value < 10)
	{
		wsprintf(buffer, L"0%I64d", value);
	}
	else
	{
		wsprintf(buffer, L"%I64d", value);
	}

	SetWindowText(hWnd, (textBefore + std::wstring(buffer) + textAfter).c_str());
}

int getLIndex(CItem* item)
{
	if (wcscmp(item->mLoai, L"Ăn uống") == 0)
	{
		return 0;
	}
	else if (wcscmp(item->mLoai, L"Di chuyển") == 0)
	{
		return 1;
	}
	else if (wcscmp(item->mLoai, L"Nhà cửa") == 0)
	{
		return 2;
	}
	else if (wcscmp(item->mLoai, L"Xe cộ") == 0)
	{
		return 3;
	}
	else if (wcscmp(item->mLoai, L"Nhu yếu phẩm") == 0)
	{
		return 4;
	}
	else
	{
		return 5;
	}
}

int xoaTD(int a, int b)
{
	if (g_totalMoney <= 0)
	{
		return -1;
	}
	if (b > Y_hh && b < Y_hh + 30)
	{
		if (a > X_hh[0] && a < X_hh[1])
		{
			return 0;
		}
		else if (a > X_hh[1] && a < X_hh[2])
		{
			return 1;
		}
		else if (a > X_hh[2] && a < X_hh[3])
		{
			return 2;
		}
		else if (a > X_hh[3] && a < X_hh[4])
		{
			return 3;
		}
		else if (a > X_hh[4] && a < X_hh[5])
		{
			return 4;
		}
		else if (a > X_hh[5] && a < X_hh[6])
		{
			return 5;
		}
	}

	return -1;
}

void ThugomRac()
{
	for (int j = 0; j < listMuc.size(); j++)
	{
		delete listMuc[j];
	}
}

void QtThemItem(HWND m_hDS)
{
	if (isAdding)
	{
		if (themItemToDS(m_hDS, g_ItemCount))
		{
			g_ItemCount++;
			isAdding = false;
			camInputWindow();
		}
	}
	else
	{
		isAdding = true;
		choInputWD();
	}
}
